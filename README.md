# switfttest
<link rel="stylesheet" href="~/lib/Font-Awesome/css/fontawesome.min.css">
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>


<i class="fas fa-sort-down"></i>
# test h1
``` mermaid
flowchart TD
    B["fab:fa-twitter for peace"]
    B-->C[fa:fa-ban forbidden]
    B-->D(fa:fa-spinner);
    B-->E(A fa:fa-camera-retro perhaps?)
```

#### <i class="fa fa-gear fa-spin fa-2x" style="color: firebrick"></i> Configuration

### <i class="fas fa-puzzle-piece" aria-hidden="true"></i> Puzzle Icon
{: #puzzle}

### <i class="fab fa-gitlab fa-fw" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i> Purple GitLab Tanuki
{: #tanuki-purple}

### <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> Orange GitLab Tanuki
{: #tanuki-orange}

<li><span class='Gitlab fontawesomeicon'></span>Gitlab</li>

<style> 
.fontawesomeicon::before {
    display: inline-block;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
 }

.Gitlab::before {
   font: var(--fa-font-brands);
    content: ' \f296';
 }</style>
